<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class Article extends Model
{
    use SoftDeletes;

    /**
     * @var array
     */
    protected $fillable = ['id','title', 'content', 'description','date'];

    /**
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at',
    ];

    /**
     * @var array
     */
    protected $dates = ['deleted_at'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public static function getAllArticles($paginate)
    {
        return DB::table('articles')
                ->join('users', 'users.id', '=', 'articles.user_id')
                ->select(
                    'articles.id',
                    'articles.title',
                    'articles.description',
                    'users.name',
                    'articles.date')
                ->whereNull('deleted_at')
                ->paginate($paginate);
    }
}

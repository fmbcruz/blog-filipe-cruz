<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\User;
use App\Article;

class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $users = User::count();
        $articles = Article::where('deleted_at', '=', null)->count();
        $authors = User::where(['author' => 1])->count();

        $collection = [
            'articles'  => $articles,
            'users'     => $users,
            'authors'   => $authors,
        ];



        return view('dashboard', compact('collection'));
    }
}

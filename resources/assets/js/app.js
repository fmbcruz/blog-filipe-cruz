
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
import Vuex from 'Vuex';
Vue.use(Vuex);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

// Vuex store
const store = new Vuex.Store({
    state: {
        item:{}
    },
    mutations: {
        setItems(state,obj) {
           state.item = obj;
        }
    }
});

Vue.component('topbar', require('./components/Topbar.vue'));
Vue.component('panel', require('./components/Panel.vue'));
Vue.component('box', require('./components/Box.vue'));
Vue.component('page', require('./components/Page.vue'));
Vue.component('table-list', require('./components/TableList.vue'));
Vue.component('breadcrumbs', require('./components/Breadcrumb.vue'));
Vue.component('modal-body', require('./components/modal/Body.vue'));
Vue.component('modal-button', require('./components/modal/Button.vue'));
Vue.component('form-head', require('./components/FormHead.vue'));
Vue.component('alert', require('./components/Alert.vue'));
Vue.component('editor', require('./components/Editor.vue'));
Vue.component('panel-group', require('./components/PanelGroup.vue'));

const app = new Vue({
    el: '#app',
    store,
    mounted: function() {
        $("#app").show();
    }
});

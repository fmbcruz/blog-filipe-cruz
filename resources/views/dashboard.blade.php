@extends('layouts.app')

@section('content')
    <page size="10">
        <panel title="Dashboard" bg="info">
            <div class="row">
                <div class="col-md-4" >
                    <box qtd="{{ $collection['articles'] }}" title="Artigos" bg="#00c0ef" icon="fa fa-newspaper-o" url="{{route('artigos.index')}}"></box>
                </div>
                <div class="col-md-4" >
                    <box qtd="{{ $collection['users'] }}" title="Usuários" bg="#dd4b39" icon="fa fa-users" url="{{route('usuarios.index')}}"></box>
                </div>
                <div class="col-md-4" >
                    <box qtd="{{ $collection['authors'] }}" title="Autores" bg="#00a65a" icon="fa fa-user" url="{{route('autores.index')}}"></box>
                </div>
            </div>
        </panel>
    </page>
@endsection

@extends('layouts.app')

@section('content')
    <page size="12">
        <breadcrumbs v-bind:list="{{$breadcrumbs}}"></breadcrumbs>
        <panel title="Autores" bg="info">

            <!-- alerta se ocorrer algum erro -->
            @if($errors->all())
                @foreach($errors->all() as $key => $val)
                    <alert message="{{$val}}"></alert>
                @endforeach
            @endif

            <table-list
                    v-bind:titles="['#', 'Nome', 'email']"
                    v-bind:items="{{ json_encode($collection) }}"
                    create="#new"
                    details="{{ route('autores.index') }}"
                    edit="{{ route('autores.index') }}" orderby="desc" ordercol="0" modal="true"
            ></table-list>
        </panel>
        <div class="float-right my-2">
            <!-- Método que monta a paginação -->
            {{ $collection->links() }}
        </div>
    </page>
    <!-- Criação -->
    <modal-body name="createModal" size="md" title="Adicionar Autor">
        <form-head id="createForm" css="" action="{{ route('autores.store') }}" method="post" enctype="" token="{{ csrf_token() }}">
            <div class="form-group">
                <label class="sr-only" for="name"> Nome </label>
                <input type="text" class="form-control" id="name" name="name" placeholder="Nome" value="{{ old('name') }}"/>
            </div>
            <div class="form-group">
                <label class="sr-only" for="email"> Email </label>
                <input type="email" class="form-control" id="email" name="email" placeholder="E-mail" value="{{ old('email') }}"/>
            </div>
            <div class="form-group">
                <label class="sr-only" for="author"> Autor </label>
                <select class="form-control" id="author" name="author">
                    <option v-if="{{ ( old('author') && old('author') == 0 ? 'selected': '' ) }}" value="0">Não</option>
                    <option v-if="{{ ( old('author') && old('author') == 1 ? 'selected': '' ) }}" value="1">Sim</option>
                </select>
            </div>
            <div class="form-group">
                <label class="sr-only" for="password"> Senha </label>
                <input type="password" class="form-control" id="password" name="password" placeholder="Senha" value="{{ old('password') }}"/>
            </div>
        </form-head>
        <span slot="buttons">
            <button form="createForm" class="btn btn-success" value="{{ old('data') }}">Salvar</button>
        </span>
    </modal-body>
    <!-- Edição -->
    <modal-body name="editModal" size="md" title="Editar Autor">
        <form-head id="editForm" css="" v-bind:action="'{{ route('autores.index') }}' + '/' + $store.state.item.id" method="put" enctype="multipart/form-data" token="{{ csrf_token() }}">
            <div class="form-group">
                <label class="sr-only" for="name"> Nome </label>
                <input type="text" class="form-control" id="name" name="name" v-model="$store.state.item.name" placeholder="Name" />
            </div>
            <div class="form-group">
                <label class="sr-only" for="email"> Email </label>
                <input type="email" class="form-control" id="email" v-model="$store.state.item.email" name="email" placeholder="E-mail" />
            </div>
            <div class="form-group">
                <label class="sr-only" for="author"> Autor </label>
                <select class="form-control" id="author" name="author" v-model="$store.state.item.author">
                    <option value="0">Não</option>
                    <option value="1">Sim</option>
                </select>
            </div>
            <div class="form-group">
                <label class="sr-only" for="password"> Senha </label>
                <input type="password" class="form-control" id="password" v-model="$store.state.item.password" name="password" placeholder="Senha" />
            </div>
        </form-head>
        <span slot="buttons">
            <button form="editForm" class="btn btn-success">Atualizar</button>
        </span>
    </modal-body>
    <!-- Detalhes -->
    <modal-body name="detailsModal" size="md" v-bind:title="$store.state.item.name" form="detailsForm">
        <p>@{{$store.state.item.email}}</p>
    </modal-body>
@endsection

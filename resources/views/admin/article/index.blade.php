@extends('layouts.app')

@section('content')
    <page size="12">
        <breadcrumbs v-bind:list="{{$breadcrumbs}}"></breadcrumbs>
        <panel title="Artigos" bg="info">

            <!-- alerta se ocorrer algum erro -->
            @if($errors->all())
                @foreach($errors->all() as $key => $val)
                    <alert message="{{$val}}"></alert>
                @endforeach
            @endif

            <table-list
                    v-bind:titles="['#', 'Título', 'Descrição', 'Autor', 'Data']"
                    v-bind:items="{{ json_encode($articles) }}"
                    create="#new"
                    details="{{ route('artigos.index') }}"
                    edit="{{ route('artigos.index') }}"
                    exclude="{{ route('artigos.index') }}"
                    token="{{ csrf_token() }}" orderby="desc" ordercol="0" modal="true"
            ></table-list>
        </panel>
        <div class="float-right my-2">
            <!-- Método que monta a paginação -->
            {{ $articles->links() }}
        </div>
    </page>
    <!-- Criação -->
    <modal-body name="createModal" size="md" title="Adicionar Artigo">
        <form-head id="createArticle" css="" action="{{ route('artigos.store') }}" method="post" enctype="" token="{{ csrf_token() }}">
            <div class="form-group">
                <label class="sr-only" for="title"> Título </label>
                <input type="text" class="form-control" id="title" name="title" placeholder="Título" value="{{ old('title') }}"/>
            </div>
            <div class="form-group">
                <label class="sr-only" for="description"> Description </label>
                <input type="text" class="form-control" id="description" name="description" placeholder="Descrição" value="{{ old('description') }}"/>
            </div>
            <div class="form-group">
                <label class="sr-only" for="date"> Data </label>
                <input type="datetime-local" class="form-control" id="date" name="date" value="{{ old('date') }}"/>
            </div>
            <div class="form-group">
                <label class="sr-only" for="contentCreate"> Conteúdo </label>
                <editor id="contentCreate" content="{{ old('content') }}"></editor>
            </div>
        </form-head>
        <span slot="buttons">
            <button form="createArticle" names="true" class="btn btn-success" value="{{ old('data') }}">Salvar</button>
        </span>
    </modal-body>
    <!-- Edição -->
    <modal-body name="editModal" size="md" title="Editar Artigo">
        <form-head id="editArticle" css="" v-bind:action="'{{ route('artigos.index') }}' + '/' + $store.state.item.id" method="put" enctype="multipart/form-data" token="{{ csrf_token() }}">
            <div class="form-group">
                <label class="sr-only" for="title"> Título </label>
                <input type="text" class="form-control" id="title" name="title" v-model="$store.state.item.title" placeholder="Título" />
            </div>
            <div class="form-group">
                <label class="sr-only" for="description"> Description </label>
                <input type="text" class="form-control" id="description" v-model="$store.state.item.description" name="description" placeholder="Descrição" />
            </div>
            <div class="form-group">
                <label class="sr-only" for="date"> Data </label>
                <input type="text" class="form-control" id="date" name="date" v-model="$store.state.item.date" readonly/>
            </div>
            <div class="form-group">
                <label class="sr-only" for="contentEdit"> Conteúdo </label>
                <editor id="contentEdit" :content="$store.state.item.content"></editor>
            </div>
        </form-head>
        <span slot="buttons">
            <button form="editArticle" class="btn btn-success">Atualizar</button>
        </span>
    </modal-body>
    <!-- Detalhes -->
    <modal-body name="detailsModal" size="md" v-bind:title="$store.state.item.title" form="detailsArticle">
        <p>@{{$store.state.item.description}}</p>
        <p>@{{$store.state.item.content}}</p>
        <p>@{{$store.state.item.date}}</p>
    </modal-body>
@endsection
